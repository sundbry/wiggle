%%%-------------------------------------------------------------------
%%% @author Heinz Nikolaus Gies <heinz@project-fifo.net>
%%% @copyright (C) 2016, Project-FiFo UG (haftungsbeschraenkt)
%%% @doc
%%%
%%% @end
%%% Created : 26 Jul 2016 by Heinz Nikolaus Gies <heinz@licenser.net>
%%%-------------------------------------------------------------------
-module(wiggle_audit).
-export([log/1, disable/1, set/3, set/2, fail/2, success/1, sets/3, sets/2,
         maybe_fail/2]).

-include("wiggle.hrl").

log(#state{
       audit      = true,
       module     = Module,
       request_id = ID,
       token      = Token,
       path       = Path,
       version    = Version,
       audit_data = AuditData,
       method     = Method}) ->
    NodeS = atom_to_binary(node(), utf8),
    ModuleS = atom_to_binary(Module, utf8),
    Event = #{<<"node">>     => NodeS,
              <<"id">>       => ID,
              <<"token">>    => Token,
              <<"version">>  => Version,
              <<"method">>   => Method,
              <<"path">>     => Path,
              <<"data">>     => AuditData,
              <<"module">>   => ModuleS},
    Time = erlang:system_time(nano_seconds),
    Events = [{Time, Event}],
    ddb_connection:events(<<"fifo-audit">>, Events),
    lager:info("[~s/~s|audit:~s] ~s ~p", [Module, ID, Token, Method, Path]);

log(_) ->
    ok.

-spec sets(binary(), binary()|number()|map()|boolean(), wiggle:state()) ->
                 wiggle:state().
sets(K, V, State) ->
    success(set(K, V, State)).

-spec set(binary(), binary()|number()|map()|boolean(), wiggle:state()) ->
                 wiggle:state().

set(K, V, State = #state{audit_data = D}) ->
    State#state{audit_data = jsxd:set(K, V, D)}.

-spec sets([{binary(), binary()|number()|map()|boolean()}],
           wiggle:state()) ->
                  wiggle:state().
sets(KVs, State) ->
    success(set(KVs, State)).

-spec set([{binary(), binary()|number()|map()|boolean()}],
          wiggle:state()) ->
                 wiggle:state().

set(KVs, State = #state{}) ->
    lists:foldl(fun({K, V}, Acc) ->
                       set(K, V, Acc)
               end, State, KVs).

disable(S) ->
    S#state{audit = false}.

-spec fail(term(), wiggle:state()) ->
                  wiggle:state().
fail(Error, State) when is_binary(Error) ->
    set([{<<"failed">>, true},
         {<<"error">>, Error}], State);
fail(ErrorS, State) when is_list(ErrorS) ->
    ErrorB = list_to_binary(ErrorS),
    fail(ErrorB, State);
fail(Error, State) ->
    ErrorS = io_lib:format("~p", [Error]),
    fail(ErrorS, State).

success(State) ->
    set(<<"failed">>, false, State).

maybe_fail(ok, State) ->
    State;
maybe_fail(E, State) ->
    fail(io_lib:format("~p", [E]), State).
