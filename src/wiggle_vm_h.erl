-module(wiggle_vm_h).

-include("wiggle.hrl").

-define(CACHE, vm).
-define(LIST_CACHE, vm_list).
-define(FULL_CACHE, vm_full_list).

-export([allowed_methods/3,
         init/1,
         get/1,
         permission_required/3,
         read/2,
         create/3,
         write/3,
         delete/2,
         schema/1,
         service_available/1]).

-behaviour(wiggle_rest_h).


-define(LIB(Call),
        Start = erlang:system_time(micro_seconds),
        case Call of
            ok ->
                ?MSniffle(?P(State1), Start),
                State2 = wiggle_audit:success(State1),
                {true, Req, State2};
            GuardCallError ->
                ?MSniffle(?P(State1), Start),
                ?ERROR("Error: ~p", [GuardCallError]),
                State2 = wiggle_audit:fail(GuardCallError, State1),
                {false, Req, State2}
        end).

init(S = #state{path = [?UUID(Vm)| _]}) ->
    wiggle_audit:set(<<"vm">>, Vm, S);

init(S) ->
    S.

service_available(#state{path = [?UUID(_Vm), <<"metrics">>| _]}) ->
    wiggle_h:service_available() andalso
        application:get_env(ddb_connection, backend) =/= undefined;
service_available(_) ->
    wiggle_h:service_available().

allowed_methods(_Version, _Token, []) ->
    [<<"GET">>, <<"POST">>];

allowed_methods(_Version, _Token, [?UUID(_Vm)]) ->
    [<<"GET">>, <<"PUT">>, <<"DELETE">>];

allowed_methods(_, _Token, [?UUID(_Vm), <<"metrics">>| _]) ->
    [<<"GET">>];

allowed_methods(_, _Token, [?UUID(_Vm), <<"config">>]) ->
    [<<"PUT">>];

allowed_methods(_, _Token, [?UUID(_Vm), <<"package">>]) ->
    [<<"PUT">>];

allowed_methods(_, _Token, [?UUID(_Vm), <<"state">>]) ->
    [<<"PUT">>];

allowed_methods(_, _Token, [?UUID(_Vm), <<"command">>]) ->
    [<<"PUT">>];

allowed_methods(_Version, _Token, [<<"dry_run">>]) ->
    [<<"PUT">>];

allowed_methods(_Version, _Token, [?UUID(_Vm), <<"hypervisor">>]) ->
    [<<"DELETE">>];

allowed_methods(_Version, _Token, [?UUID(_Vm), <<"owner">>]) ->
    [<<"PUT">>];

allowed_methods(_Version, _Token, [?UUID(_Vm), <<"services">>]) ->
    [<<"PUT">>];

allowed_methods(_Version, _Token, [?UUID(_Vm), <<"metadata">>|_]) ->
    [<<"PUT">>, <<"DELETE">>];


allowed_methods(_Version, _Token, [?UUID(_Vm), <<"hostname">>, _Nic]) ->
    [<<"PUT">>];

allowed_methods(_Version, _Token, [?UUID(_Vm), <<"nics">>, _Mac]) ->
    [<<"PUT">>, <<"DELETE">>];

allowed_methods(_Version, _Token, [?UUID(_Vm), <<"nics">>]) ->
    [<<"POST">>];

allowed_methods(_Version, _Token, [?UUID(_Vm), <<"snapshots">>, _ID]) ->
    [<<"PUT">>, <<"DELETE">>];

allowed_methods(_Version, _Token, [?UUID(_Vm), <<"snapshots">>]) ->
    [<<"POST">>];

allowed_methods(_Version, _Token, [?UUID(_Vm), <<"fw_rules">>, _ID]) ->
    [<<"DELETE">>]; %% We might need to add PUT later.

allowed_methods(_Version, _Token, [?UUID(_Vm), <<"fw_rules">>]) ->
    [<<"POST">>];

allowed_methods(_Version, _Token, [?UUID(_Vm), <<"backups">>, _ID]) ->
    [<<"PUT">>, <<"DELETE">>, <<"GET">>];

allowed_methods(_Version, _Token, [?UUID(_Vm), <<"backups">>]) ->
    [<<"POST">>].

get(State = #state{path = [?UUID(Vm), <<"backups">>, Snap]}) ->
    case wiggle_vm_h:get(State#state{path=[?UUID(Vm)]}) of
        {ok, Obj} ->
            case jsxd:get([Snap], ft_vm:backups(Obj)) of
                undefined -> not_found;
                {ok, _} -> {ok, Obj}
            end;
        E ->
            E
    end;

get(State = #state{path = [?UUID(Vm), <<"snapshots">>, Snap]}) ->
    case wiggle_vm_h:get(State#state{path=[?UUID(Vm)]}) of
        {ok, Obj} ->
            case jsxd:get([Snap], ft_vm:snapshots(Obj)) of
                undefined -> not_found;
                {ok, _} -> {ok, Obj}
            end;
        E ->
            E
    end;

get(State = #state{path = [?UUID(Vm), <<"nics">>, Mac]}) ->
    case wiggle_vm_h:get(State#state{path=[?UUID(Vm)]}) of
        {ok, Obj} ->
            Macs = [jsxd:get([<<"mac">>], <<>>, N) ||
                       N <- jsxd:get([<<"networks">>], [], ft_vm:config(Obj))],
            case lists:member(Mac, Macs) of
                true ->
                    {ok, Obj};
                _ ->
                    not_found
            end;
        E ->
            E
    end;

get(State = #state{path = [?UUID(Vm), <<"hostname">>, Nic]}) ->
    case wiggle_vm_h:get(State#state{path=[?UUID(Vm)]}) of
        {ok, Obj} ->
            Nics = [jsxd:get([<<"interface">>], <<>>, N) ||
                       N <- jsxd:get([<<"networks">>], [], ft_vm:config(Obj))],
            case lists:member(Nic, Nics) of
                true ->
                    {ok, Obj};
                _ ->
                    not_found
            end;
        E ->
            E
    end;

get(State = #state{path = [?UUID(Vm), <<"fw_rules">>, IDB]}) ->
    case wiggle_vm_h:get(State#state{path=[?UUID(Vm)]}) of
        {ok, Obj} ->
            ID = binary_to_integer(IDB),
            case find_rule(ID, Obj) of
                {ok, _} ->
                    {ok, Obj};
                _ ->
                    not_found
            end;
        E ->
            E
    end;

get(#state{path = [?UUID(_Vm), <<"metrics">>]}) ->
    {ok, erlang:system_time(micro_seconds)};

get(State = #state{path = [?UUID(Vm) | _]}) ->
    Start = erlang:system_time(micro_seconds),
    R = case application:get_env(wiggle, vm_ttl) of
            {ok, {TTL1, TTL2}} ->
                wiggle_h:timeout_cache_with_invalid(
                  ?CACHE, Vm, TTL1, TTL2, not_found,
                  fun() -> ls_vm:get(Vm) end);
            _ ->
                ls_vm:get(Vm)
        end,
    ?MSniffle(?P(State), Start),
    R;

get(_State) ->
    not_found.

permission_required(put, [<<"dry_run">>], _) ->
    {ok, [<<"cloud">>, <<"vms">>, <<"create">>]};

permission_required(get, [], _) ->
    {ok, [<<"cloud">>, <<"vms">>, <<"list">>]};

permission_required(post, [], _) ->
    {ok, [<<"cloud">>, <<"vms">>, <<"create">>]};

permission_required(get, [?UUID(Vm)], _) ->
    {ok, [<<"vms">>, Vm, <<"get">>]};

permission_required(get, [?UUID(Vm), <<"metrics">> | _], _) ->
    {ok, [<<"vms">>, Vm, <<"get">>]};

permission_required(delete, [?UUID(Vm)], _) ->
    {ok, [<<"vms">>, Vm, <<"delete">>]};

permission_required(delete, [?UUID(Vm), <<"hypervisor">>], _) ->
    {ok, [<<"vms">>, Vm, <<"delete">>]};

permission_required(put, [?UUID(Vm), <<"hostname">>, _], _) ->
    {ok, [<<"vms">>, Vm, <<"edit">>]};

permission_required(post, [?UUID(Vm), <<"nics">>], _) ->
    {ok, [<<"vms">>, Vm, <<"edit">>]};

permission_required(put, [?UUID(Vm), <<"nics">>, _], _) ->
    {ok, [<<"vms">>, Vm, <<"edit">>]};

permission_required(delete, [?UUID(Vm), <<"nics">>, _], _) ->
    {ok, [<<"vms">>, Vm, <<"edit">>]};

permission_required(post, [?UUID(Vm), <<"snapshots">>], _) ->
    {ok, [<<"vms">>, Vm, <<"snapshot">>]};

permission_required(post, [?UUID(Vm), <<"fw_rules">>], _) ->
    {ok, [<<"vms">>, Vm, <<"edit">>]};

permission_required(delete, [?UUID(Vm), <<"fw_rules">>, _FWID], _) ->
    {ok, [<<"vms">>, Vm, <<"edit">>]};

permission_required(put, [?UUID(Vm), <<"services">>], _) ->
    {ok, [<<"vms">>, Vm, <<"edit">>]};

permission_required(get, [?UUID(Vm), <<"backups">>, _], _) ->
    {ok, [<<"vms">>, Vm, <<"get">>]};

permission_required(post, [?UUID(Vm), <<"backups">>], _) ->
    {ok, [<<"vms">>, Vm, <<"backup">>]};

permission_required(put, [?UUID(_Vm), <<"owner">>], #state{body = undefiend}) ->
    {error, needs_decode};

permission_required(put, [?UUID(Vm), <<"owner">>], #state{body = Decoded}) ->
    case Decoded of
        #{<<"org">> := Owner} ->
            {multiple,
             [[<<"vms">>, Vm, <<"edit">>],
              [<<"orgs">>, Owner, <<"edit">>]]};
        _ ->
            {ok, [<<"vms">>, Vm, <<"edit">>]}
    end;

permission_required(put, _, #state{body = undefiend}) ->
    {error, needs_decode};

permission_required(put, [?UUID(_Vm), <<"state">>], #state{body = undefined}) ->
    {error, needs_decode};

permission_required(put, [?UUID(Vm), <<"state">>],
                    #state{body = #{<<"action">> := Act}}) ->
    {ok, [<<"vms">>, Vm, Act]};


permission_required(put, [?UUID(Vm), <<"command">>], _) ->
    {ok, [<<"vms">>, Vm, <<"command">>]};

permission_required(put, [?UUID(Vm), <<"config">>], _ ) ->
    {ok, [<<"vms">>, Vm, <<"edit">>]};

permission_required(put, [?UUID(Vm), <<"package">>], _) ->
    {ok, [<<"vms">>, Vm, <<"edit">>]};

permission_required(put, [?UUID(Vm), <<"snapshots">>, _Snap],
                    #state{body = #{<<"action">> := <<"rollback">>}}) ->
    {ok, [<<"vms">>, Vm, <<"rollback">>]};

permission_required(put, [?UUID(Vm), <<"snapshots">>, _Snap], _) ->
    {ok, [<<"vms">>, Vm, <<"edit">>]};

permission_required(delete, [?UUID(Vm), <<"snapshots">>, _Snap], _) ->
    {ok, [<<"vms">>, Vm, <<"snapshot_delete">>]};

permission_required(put, [?UUID(Vm), <<"backups">>, _Snap],
                    #state{body = #{<<"action">> := <<"rollback">>}}) ->
    {ok, [<<"vms">>, Vm, <<"rollback">>]};

permission_required(put, [?UUID(Vm), <<"backups">>, _Snap],
                    #state{body = #{<<"action">> := <<"restore">>}}) ->
    {ok, [<<"vms">>, Vm, <<"rollback">>]};

permission_required(put, [?UUID(Vm), <<"backups">>, _Snap], _) ->
    {ok, [<<"vms">>, Vm, <<"edit">>]};

permission_required(delete, [?UUID(Vm), <<"backups">>, _Snap], _) ->
    {ok, [<<"vms">>, Vm, <<"backup_delete">>]};

permission_required(put, [?UUID(Vm), <<"metadata">> | _], _) ->
    {ok, [<<"vms">>, Vm, <<"edit">>]};

permission_required(delete, [?UUID(Vm), <<"metadata">> | _], _) ->
    {ok, [<<"vms">>, Vm, <<"edit">>]};

permission_required(_Method, _Path, _State) ->
    undefined.


%%--------------------------------------------------------------------
%% Schema
%%--------------------------------------------------------------------

%% Creates a VM
schema(#state{method = <<"POST">>, path = []}) ->
    vm_create;

%% Creates a snapshot
schema(#state{method = <<"PUT">>, path = [?UUID(_Vm), <<"snapshots">>]}) ->
    vm_snapshot;

%% Adds a firewall rule
schema(#state{method = <<"PUT">>, path = [?UUID(_Vm), <<"fw_rules">>]}) ->
    vm_fw_rule;

%% create a backup
schema(#state{method = <<"PUT">>, path = [?UUID(_Vm), <<"backups">>]}) ->
    vm_backup;

%% adds a nic
schema(#state{method = <<"PUT">>, path = [?UUID(_Vm), <<"nics">>]}) ->
    vm_add_nic;

%% Sets the hostname
schema(#state{method = <<"PUT">>, path = [?UUID(_Vm), <<"hostname">>, _Nic]}) ->
    vm_change_hostname;

%% Dry run
schema(#state{method = <<"PUT">>, path = []}) ->
    vm_create;

%% Changes a VM state
schema(#state{method = <<"PUT">>, path = [?UUID(_Vm), <<"state">>]}) ->
    vm_update_state;

%% Changes a VM state
schema(#state{method = <<"PUT">>, path = [?UUID(_Vm), <<"command">>]}) ->
    vm_console_execute;

%% Updates a VM Config, we don't have validation that in the V! api
schema(#state{method = <<"PUT">>, path = [?UUID(_Vm), <<"config">>]}) ->
    vm_update_config;

schema(#state{method = <<"PUT">>, path = [?UUID(_Vm), <<"package">>]}) ->
    vm_update_package;

%% Snapshots
schema(#state{method = <<"POST">>,
              path = [?UUID(_Vm), <<"snapshots">>, ?UUID(_Snap)]}) ->
    vm_rollback_snapshot;

%% Backups
schema(#state{method = <<"POST">>,
              body = #{<<"action">> := <<"rollback">>},
              path = [?UUID(_Vm), <<"backups">>, ?UUID(_Backup)]}) ->
    vm_rollback_backup;

schema(#state{method = <<"POST">>,
              body = #{<<"action">> := <<"restore">>},
              path = [?UUID(_Vm), <<"backups">>, ?UUID(_Backup)]}) ->
    vm_restore_backup;

%% State change
schema(#state{method = <<"POST">>, path = [?UUID(_Vm), <<"services">>]}) ->
    vm_service_change;

schema(_State) ->
    none.

%%--------------------------------------------------------------------
%% GET
%%--------------------------------------------------------------------
read(Req, State = #state{path = []}) ->
    State1 = wiggle_audit:sets(<<"action">>, <<"list">>, State),
    wiggle_h:list(<<"vms">>,
                  fun ls_vm:stream/3,
                  fun ft_vm:uuid/1,
                  fun to_json/1,
                  Req, State1);

read(Req, State = #state{path = [?UUID(_Vm)], obj = Obj}) ->
    State1 = wiggle_audit:sets(<<"action">>, <<"get">>, State),
    {to_json(Obj), Req, State1};

read(Req, State = #state{path = [?UUID(_Vm), <<"backups">>, BID], obj = Obj}) ->
    State1 = wiggle_audit:sets(<<"action">>, <<"verify_backup">>, State),
    Backups = ft_vm:backups(Obj),
    {ok, Files} = jsxd:get([BID, <<"files">>], Backups),
    {ok, {Host, Port, AKey, SKey, Bucket}} = libsniffle:s3(snapshot),
    Config = [{host, Host},
              {port, Port},
              {chunk_size, ?CHUNK_SIZE},
              {bucket, Bucket},
              {access_key, AKey},
              {secret_key, SKey}],
    Res = [verify_file(File, Config) || File <- maps:to_list(Files)],
    {Res, Req, State1};

read(Req, State = #state{path = [?UUID(Vm), <<"metrics">>]}) ->
    State1 = wiggle_audit:disable(State),
    {QS, Req1} = cowboy_req:qs_vals(Req),
    case perf(Vm, QS) of
        {ok, JSON} ->
            {JSON, Req1, State1};
        {error, no_results} ->
            {ok, Req2} = cowboy_req:reply(
                           503, [], <<"Empty result set">>, Req1),
            {halt, Req2, State1};
        {error, bad_qs} ->
            {ok, Req2} = cowboy_req:reply(
                           400, [], <<"bad query string">>, Req1),
            {halt, Req2, State1};
        {error, bad_resolution} ->
            {ok, Req2} = cowboy_req:reply(
                           400, [], <<"bad resolution">>, Req1),
            {halt, Req2, State1}
    end.


%%--------------------------------------------------------------------
%% POST
%%--------------------------------------------------------------------

create(Req, State = #state{path = [], version = Version, token = Token},
       Decoded) ->
    {ok, Dataset} = jsxd:get(<<"dataset">>, Decoded),
    {ok, Package} = jsxd:get(<<"package">>, Decoded),
    {ok, Config} = jsxd:get(<<"config">>, Decoded),
    State1 = wiggle_audit:set([{<<"action">>, <<"create">>},
                               {<<"dataset">>, Dataset},
                               {<<"package">>, Package}],
                              State),
    %% If the creating user has advanced_create permissions they can pass
    %% 'requirements' as part of the config, if they lack the permission
    %% it simply gets removed.
    Reqs = jsxd:get(<<"requirements">>, [], Config),
    Config1 = case can_use_rules(Reqs, Token) of
                  true ->
                      Config;
                  _ ->
                      ?WARNING("[create] Advanced crate rules ignored"
                               " for VM due to lacking permissions."),
                      jsxd:set(<<"requirements">>, [], Config)
              end,
    Start = erlang:system_time(micro_seconds),
    try
        Config2 = jsxd:set(<<"owner">>, user(State1), Config1),
        {ok, UUID} = ls_vm:create(Package, Dataset, Config2),
        State2 = wiggle_audit:sets(<<"vm">>, UUID, State1),
        e2qc:teardown(?LIST_CACHE),
        e2qc:teardown(?FULL_CACHE),
        ?MSniffle(?P(State2), Start),
        {{true, <<"/api/", Version/binary, "/vms/", UUID/binary>>},
         Req, State2#state{body = Decoded}}
    catch
        G:E ->
            end_end_error(
              {G, E}, Start, State, Req,
              "Error creating VM(~p): ~p / ~p", [Decoded, G, E])
    end;

create(Req, State = #state{path = [?UUID(Vm), <<"snapshots">>],
                           version = Version}, Decoded) ->
    Comment = jsxd:get(<<"comment">>, <<"">>, Decoded),
    Start = erlang:system_time(micro_seconds),
    {ok, UUID} = ls_vm:snapshot(Vm, Comment),
    State1 = wiggle_audit:sets([{<<"uuid">>, UUID},
                                {<<"action">>, <<"snapshot">>}], State),
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State1), Start),
    {{true, <<"/api/", Version/binary, "/vms/", Vm/binary>>},
     Req, State1#state{body = Decoded}};

create(Req, State = #state{path = [?UUID(Vm), <<"fw_rules">>],
                           version = Version}, RuleJSON) ->
    Start = erlang:system_time(micro_seconds),
    Rule = ft_vm:json_to_fw_rule(RuleJSON),
    State1 = wiggle_audit:sets([{<<"rule">>, RuleJSON},
                                {<<"action">>, <<"add_rule">>}], State),
    ls_vm:add_fw_rule(Vm, Rule),
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State1), Start),
    {{true, <<"/api/", Version/binary, "/vms/", Vm/binary>>},
     Req, State1#state{body = RuleJSON}};

create(Req, State = #state{path = [?UUID(Vm), <<"backups">>],
                           version = Version}, Decoded) ->
    Comment = jsxd:get(<<"comment">>, <<"">>, Decoded),
    Opts = [xml],
    Start = erlang:system_time(micro_seconds),
    NewPath = <<"/api/", Version/binary, "/vms/", Vm/binary>>,
    case jsxd:get(<<"parent">>, Decoded) of
        {ok, Parent} ->
            Opts1 = case jsxd:get(<<"delete">>, false, Decoded) of
                        true ->
                            [{delete, parent} | Opts];
                        false ->
                            Opts
                    end,
            Reply = ls_vm:incremental_backup(Vm, Parent, Comment, Opts1),
            e2qc:evict(?CACHE, Vm),
            e2qc:teardown(?FULL_CACHE),
            ?MSniffle(?P(State), Start),
            case Reply of
                {ok, UUID} ->
                    State1 = wiggle_audit:sets([{<<"uuid">>, UUID},
                                                {<<"action">>, <<"ibackup">>}],
                                               State),
                    {{true, NewPath}, Req, State1#state{body = Decoded}};
                {error, not_supported} ->
                    {ok, Req1} = cowboy_req:reply(
                                   501, [], <<"not supported">>, Req),
                    {halt, Req1, State}
            end;
        _ ->
            Opts1 = case jsxd:get(<<"delete">>, false, Decoded) of
                        true ->
                            [delete | Opts];
                        false ->
                            Opts
                    end,
            Reply = ls_vm:full_backup(Vm, Comment, Opts1),
            case Reply of
                {ok, UUID} ->
                    State1 = wiggle_audit:sets([{<<"uuid">>, UUID},
                                                {<<"action">>, <<"fbackup">>}],
                                               State),
                    {{true, NewPath}, Req, State1#state{body = Decoded}};
                {error, not_supported} ->
                    {ok, Req1} = cowboy_req:reply(
                                   501, [], <<"not supported">>, Req),
                    {halt, Req1, State}
            end
    end;

create(Req, State = #state{path = [?UUID(Vm), <<"nics">>], version = Version},
       #{<<"network">> := Network}) ->
    Start = erlang:system_time(micro_seconds),
    State1 = wiggle_audit:sets([{<<"network">>, Network},
                                {<<"action">>, <<"add_nic">>}], State),
    case ls_vm:add_nic(Vm, Network) of
        ok ->
            ?MSniffle(?P(State), Start),
            e2qc:evict(?CACHE, Vm),
            e2qc:teardown(?FULL_CACHE),
            {{true, <<"/api/", Version/binary, "/vms/", Vm/binary>>},
             Req, State1};
        {error, not_stopped} ->
            {ok, Req1} = cowboy_req:reply(412, [], <<"VM Running">>, Req),
            ?ERROR("Could not add nic, vm running."),
            State2 = wiggle_audit:fail("running", State1),
            {halt, Req1, State2};
        E ->
            end_end_error(
              E, Start, State, Req,
              "Error adding nic to VM(~p) on network(~p) / ~p",
              [?UUID(Vm), Network, E])
    end.

%%--------------------------------------------------------------------
%% PUT
%%--------------------------------------------------------------------

write(Req, State = #state{path = [?UUID(Vm), <<"hostname">>, Nic]},
      #{<<"hostname">> := Hostname}) ->
    Start = erlang:system_time(micro_seconds),
    State1 = wiggle_audit:sets([{<<"nic">>, Nic},
                                {<<"hostname">>, Hostname},
                                {<<"action">>, <<"change_hostname">>}], State),
    case ls_vm:hostname(Vm, Nic, Hostname) of
        ok ->
            ?MSniffle(?P(State), Start),
            e2qc:evict(?CACHE, Vm),
            e2qc:teardown(?FULL_CACHE),
            {true, Req, State1};
        E ->
            end_end_error(
              E, Start, State, Req,
              "Error changing hostname on VM(~p) for network(~p): ~p",
              [?UUID(Vm), Nic, E])
    end;

write(Req, State = #state{path = [<<"dry_run">>], token = Token}, Decoded) ->
    ?INFO("Starting dryrun."),
    try
        {ok, Dataset} = jsxd:get(<<"dataset">>, Decoded),
        {ok, Package} = jsxd:get(<<"package">>, Decoded),
        {ok, Config} = jsxd:get(<<"config">>, Decoded),
        %% If the creating user has advanced_create permissions they can pass
        %% 'requirements' as part of the config, if they lack the permission
        %% it simply gets removed.
        Config1 = case libsnarl:allowed(
                         Token,
                         [<<"cloud">>, <<"vms">>, <<"advanced_create">>]) of
                      true ->
                          Config;
                      _ ->
                          jsxd:set(<<"requirements">>, [], Config)
                  end,
        try
            {ok, User} = ls_user:get(Token),
            Owner = ft_user:uuid(User),
            case ls_vm:dry_run(Package, Dataset,
                               jsxd:set(<<"owner">>, Owner, Config1)) of
                {ok, success} ->
                    {true, Req, State#state{body = Decoded}};
                E ->
                    ?WARNING("Dry run failed with: ~p.", [E]),
                    {false, Req, State#state{body = Decoded}}
            end
        catch
            _G:_E ->
                {false, Req, State}
        end
    catch
        _G1:_E1 ->
            {false, Req, State}
    end;

write(Req, State = #state{path = [?UUID(UUID), <<"command">>], obj = VM},
      Body) ->
    {ok, Cmd} = jsxd:get([<<"command">>], Body),
    Args = jsxd:get([<<"args">>], [], Body),
    FullCmd = [Cmd | Args],
    State1 = wiggle_audit:set([{<<"command">>, FullCmd},
                               {<<"action">>, <<"execute_command">>}], State),
    case ft_vm:state(VM) of
        <<"running">> ->
            Hv = ft_vm:hypervisor(VM),
            {ok, H} = ls_hypervisor:get(Hv),
            {Host, Port} = ft_hypervisor:endpoint(H),
            {ok, C, T} = libchunter:execute(Host, Port, UUID, FullCmd, <<>>,
                                            fun fold_reply/2),
            Reply = #{<<"exit_code">> => C,
                      <<"output">> => T},
            %% TODO: distinguish between msgpack and json
            Bin = jsone:encode(Reply),
            Req1 = cowboy_req:set_resp_body(Bin, Req),
            State2 = wiggle_audit:sets(<<"exit_code">>, C, State1),
            {true, Req1, State2};
        E ->
            {ok, Req1} = cowboy_req:reply(412, [], <<"VM not running">>, Req),
            State2 = wiggle_audit:fail(E, State1),
            {halt, Req1, State2}
    end;

write(Req, State = #state{path = [?UUID(Vm), <<"services">>]},
      #{<<"action">> := <<"enable">>,
        <<"service">> :=Service}) ->
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    Reply = ls_vm:service_enable(Vm, Service),
    State1 = wiggle_audit:sets([{<<"action">>,  <<"service">>},
                                {<<"service">>, Service},
                                {<<"op">>, <<"enable">>}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2};

write(Req, State = #state{path = [?UUID(Vm), <<"services">>]},
      #{<<"action">> := <<"disable">>,
        <<"service">> := Service}) ->
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    Reply = ls_vm:service_disable(Vm, Service),
    State1 = wiggle_audit:sets([{<<"action">>,  <<"service">>},
                                {<<"service">>, Service},
                                {<<"op">>, <<"disable">>}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2};

write(Req, State = #state{path = [?UUID(Vm), <<"services">>]},
      #{<<"action">> := <<"clear">>,
        <<"service">> := Service}) ->
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    Reply = ls_vm:service_clear(Vm, Service),
    State1 = wiggle_audit:sets([{<<"action">>,  <<"service">>},
                                {<<"service">>, Service},
                                {<<"op">>, <<"clear">>}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2};

write(Req, State = #state{path = [?UUID(Vm), <<"services">>]},
      #{<<"action">> := <<"refresh">>,
        <<"service">> := Service}) ->
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    Reply = ls_vm:service_refresh(Vm, Service),
    State1 = wiggle_audit:sets([{<<"action">>,  <<"service">>},
                                {<<"service">>, Service},
                                {<<"op">>, <<"refresh">>}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2};

write(Req, State = #state{path = [?UUID(Vm), <<"services">>]},
      #{<<"action">> := <<"restart">>,
        <<"service">> := Service}) ->
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    Reply = ls_vm:service_restart(Vm, Service),
    State1 = wiggle_audit:sets([{<<"action">>,  <<"service">>},
                                {<<"service">>, Service},
                                {<<"op">>, <<"restart">>}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2};

write(Req, State = #state{path = [?UUID(Vm), <<"owner">>]},
      #{<<"org">> := Org}) ->
    Start = erlang:system_time(micro_seconds),
    State1 = wiggle_audit:sets([{<<"new_owner">>, Org},
                               {<<"action">>, <<"change_owner">>}], State),
    case ls_org:get(Org) of
        {ok, _} ->
            e2qc:evict(?CACHE, Vm),
            e2qc:teardown(?FULL_CACHE),
            R = ls_vm:owner(user(State), Vm, Org),
            ?MSniffle(?P(State), Start),
            State2 = wiggle_audit:maybe_fail(R, State1),
            {R =:= ok, Req, State2};
        E ->
            ?MSniffle(?P(State), Start),
            ?ERROR("Error trying to assign org ~p since it does not "
                   "seem to exist", [Org]),
            {ok, Req1} = cowboy_req:reply(404, Req),
            ?ERROR("Could not change owner: ~p", [E]),
            State2 = wiggle_audit:fail("not_found", State1),
            {halt, Req1, State2}
    end;

write(Req, State = #state{path = [?UUID(Vm), <<"nics">>, Mac]},
      #{<<"primary">> := true}) ->
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    State1 = wiggle_audit:set([{<<"nic">>, Mac},
                               {<<"action">>, <<"primary_nic">>}], State),
    ?LIB(ls_vm:primary_nic(Vm, Mac));

write(Req, State = #state{path = [?UUID(Vm), <<"metadata">> | Path]},
      O) when is_map(O) ->
    [{K, V}] = maps:to_list(O),
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    %% We need to do that since the lib marcro expects State1
    State1 = State,
    ?LIB(ls_vm:set_metadata(Vm,  [{Path ++ [K], V}]));

%%--------------------------------------------------------------------
%% Power State Changes
%%--------------------------------------------------------------------

write(Req, State = #state{path = [?UUID(Vm), <<"state">>]},
      #{<<"action">> := <<"start">>}) ->
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    State1 = wiggle_audit:set(<<"action">>, <<"start">>, State),
    ?LIB(ls_vm:start(Vm));

write(Req, State = #state{path = [?UUID(Vm), <<"state">>]},
      #{<<"action">> := <<"stop">>}) ->
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    State1 = wiggle_audit:set(<<"action">>, <<"stop">>, State),
    ?LIB(ls_vm:stop(Vm));

write(Req, State = #state{path = [?UUID(Vm), <<"state">>]},
      #{<<"action">> := <<"stop">>, <<"force">> := true}) ->
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    State1 = wiggle_audit:set([{<<"action">>, <<"stop">>},
                               {<<"force">>, true}], State),
    ?LIB(ls_vm:stop(Vm, [force]));

write(Req, State = #state{path = [?UUID(Vm), <<"state">>]},
      #{<<"action">> := <<"reboot">>}) ->
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    State1 = wiggle_audit:set(<<"action">>, <<"reboot">>, State),
    ?LIB(ls_vm:reboot(Vm));

write(Req, State = #state{path = [?UUID(Vm), <<"state">>]},
      #{<<"action">> := <<"reboot">>, <<"force">> := true}) ->
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    State1 = wiggle_audit:set([{<<"action">>, <<"reboot">>},
                               {<<"force">>, true}], State),
    ?LIB(ls_vm:reboot(Vm, [force]));

%%--------------------------------------------------------------------
%% VM Update
%%--------------------------------------------------------------------

write(Req, State = #state{path = [?UUID(Vm), <<"config">>]},
      Config) ->
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    State1 = wiggle_audit:set([{<<"action">>, <<"change_config">>},
                               {<<"config">>, Config}], State),
    ?LIB(ls_vm:update(user(State), Vm, undefined, Config));

write(Req, State = #state{path = [?UUID(Vm), <<"package">>]},
      #{<<"package">> := Package}) ->
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    State1 = wiggle_audit:set([{<<"action">>, <<"change_package">>},
                               {<<"package">>, Package}], State),
    ?LIB(ls_vm:update(user(State), Vm, Package, #{}));

%%--------------------------------------------------------------------
%% Snapshots
%%--------------------------------------------------------------------

write(Req, State = #state{path = [?UUID(Vm), <<"snapshots">>, UUID]},
      #{<<"action">> := <<"rollback">>}) ->
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    State1 = wiggle_audit:set([{<<"action">>, <<"rollback_snapshot">>},
                               {<<"snapshot">>, UUID}], State),
    ?LIB(ls_vm:rollback_snapshot(Vm, UUID));

%%--------------------------------------------------------------------
%% backups
%%--------------------------------------------------------------------
write(Req, State = #state{version_i = 2,
                          path = [?UUID(Vm), <<"backups">>, UUID]},
      #{<<"action">> := <<"rollback">>,
        <<"hypervisor">> := Hypervisor}) ->
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    Rule = [
            {<<"attribute">>, <<"uuid">>},
            {<<"condition">>, <<"=:=">>},
            {<<"value">>,     Hypervisor},
            {<<"weight">>,    <<"must">>}
           ],
    State1 = wiggle_audit:set([{<<"action">>, <<"rollback_backup">>},
                               {<<"backup">>, UUID},
                               {<<"rules">>, [Rule]}], State),
    ?LIB(ls_vm:restore_backup(user(State), Vm, UUID, [Rule]));

write(Req, State = #state{version_i = 3, token = Token,
                          path = [?UUID(Vm), <<"backups">>, UUID]},
      #{<<"action">> := <<"restore">>} = Rest) ->
    Rules = jsxd:get(<<"rules">>, [], Rest),
    Rules1 = case can_use_rules(Rules, Token) of
                 true ->
                     Rules;
                 _ ->
                     ?WARNING("[restore:~s] Advanced restore rules ignored"
                              " due to lacking permissions.", [Vm]),
                     []
             end,
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    State0 = wiggle_audit:set([{<<"action">>, <<"rollback_backup">>},
                               {<<"backup">>, UUID},
                               {<<"rules">>, Rules}], State),
    case jsxd:get(<<"package">>, Rest) of
        {ok, Package} ->
            State1 = wiggle_audit:set(<<"package">>, Package, State0),
            ?LIB(ls_vm:restore_backup(user(State), Vm, UUID, Rules1, Package));
        _ ->
            %% We need to do that since the lib marcro expects State1
            State1 = State0,
            ?LIB(ls_vm:restore_backup(user(State), Vm, UUID, Rules1))
    end;

write(Req, State = #state{path = [?UUID(Vm), <<"backups">>, UUID]},
      #{<<"action">> := <<"rollback">>}) ->
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    State1 = wiggle_audit:set([{<<"action">>, <<"rollback_backup">>},
                               {<<"backup">>, UUID},
                               {<<"rules">>, []}], State),
    ?LIB(ls_vm:restore_backup(Vm, UUID));

write(Req, State, _Body) ->
    ?ERROR("Unknown PUT request: ~p~n.", [State]),
    {false, Req, State}.

%%--------------------------------------------------------------------
%% DEETE
%%--------------------------------------------------------------------

delete(Req, State = #state{path = [?UUID(Vm), <<"snapshots">>, UUID]}) ->
    Start = erlang:system_time(micro_seconds),
    ok = ls_vm:delete_snapshot(Vm, UUID),
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    State1 = wiggle_audit:sets([{<<"action">>, <<"delete_snapshot">>},
                               {<<"snapshot">>, UUID}], State),
    {true, Req, State1};

delete(Req, State = #state{path = [?UUID(Vm), <<"fw_rules">>, RuleIDs],
                           obj = Obj}) ->
    Start = erlang:system_time(micro_seconds),
    RuleID = binary_to_integer(RuleIDs),
    {ok, Rule} = find_rule(RuleID, Obj),
    RuleJSON = ft_vm:fw_rule_to_json(Rule),
    ok = ls_vm:remove_fw_rule(Vm, Rule),
    State1 = wiggle_audit:sets([{<<"action">>, <<"remove_fw_rule">>},
                               {<<"rule">>, RuleJSON}], State),
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State1), Start),
    {true, Req, State1};

delete(Req, State = #state{path = [?UUID(Vm), <<"backups">>, UUID],
                           body= #{<<"location">> := <<"hypervisor">>}}) ->
    Start = erlang:system_time(micro_seconds),
    ok = ls_vm:delete_backup(Vm, UUID, hypervisor),
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    State1 =
        wiggle_audit:sets([{<<"action">>, <<"remove_backup_local">>},
                           {<<"backup">>, UUID}], State),

    {true, Req, State1};

delete(Req, State = #state{path = [?UUID(Vm), <<"backups">>, UUID]}) ->
    Start = erlang:system_time(micro_seconds),
    ok = ls_vm:delete_backup(Vm, UUID, cloud),
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    State1 = wiggle_audit:sets([{<<"action">>, <<"remove_backup">>},
                                {<<"backup">>, UUID}], State),
    {true, Req, State1};

delete(Req, State = #state{path = [?UUID(Vm), <<"nics">>, Mac]}) ->
    Start = erlang:system_time(micro_seconds),
    ok = ls_vm:remove_nic(Vm, Mac),
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    State1 = wiggle_audit:sets([{<<"action">>, <<"remove_nic">>},
                                {<<"nic">>, Mac}], State),
    {true, Req, State1};

delete(Req, State = #state{path = [?UUID(Vm)],
                           body= #{<<"location">> := <<"hypervisor">>}}) ->
    Start = erlang:system_time(micro_seconds),
    ok = ls_vm:store(user(State), Vm),
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    State1 = wiggle_audit:sets([{<<"action">>, <<"store_vm">>}], State),
    {true, Req, State1};

delete(Req, State = #state{path = [?UUID(Vm), <<"hypervisor">>]}) ->
    Start = erlang:system_time(micro_seconds),
    ok = ls_vm:store(user(State), Vm),
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    State1 = wiggle_audit:sets([{<<"action">>, <<"store_vm">>}], State),
    {true, Req, State1};

delete(Req, State = #state{path = [?UUID(Vm)]}) ->
    Start = erlang:system_time(micro_seconds),
    State1 = wiggle_audit:sets([{<<"action">>, <<"store_vm">>}], State),
    case ls_vm:delete(user(State), Vm) of
        ok ->
            e2qc:evict(?CACHE, Vm),
            e2qc:teardown(?LIST_CACHE),
            e2qc:teardown(?FULL_CACHE),
            ?MSniffle(?P(State), Start),
            {true, Req, State1};
        {error, creating} ->
            {ok, Req1} = cowboy_req:reply(423, Req),
            ?ERROR("Could not delete: locked"),
            State2 = wiggle_audit:fail("creating", State1),
            {halt, Req1, State2}
    end;

delete(Req, State = #state{path = [?UUID(Vm), <<"metadata">> | Path]}) ->
    Start = erlang:system_time(micro_seconds),
    ls_vm:set_metadata(Vm, [{Path, delete}]),
    e2qc:evict(?CACHE, Vm),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    {true, Req, State}.

user(#state{token = Token}) ->
    {ok, User} = ls_user:get(Token),
    ft_user:uuid(User).

to_json(VM) ->
    jsxd:update(<<"fw_rules">>,
                fun (Rules) ->
                        [ Rule#{<<"id">> => erlang:phash2(Rule)} ||
                            Rule <- Rules]
                end, ft_vm:to_json(VM)).

find_rule(ID, VM) ->
    Rules = jsxd:get(<<"fw_rules">>, [], ft_vm:to_json(VM)),
    Found = lists:filter(fun(Rule) ->
                                 ID == erlang:phash2(Rule)
                         end, Rules),
    case Found of
        [Rule] ->
            {ok, ft_vm:json_to_fw_rule(Rule)};
        _ ->
            {error, oh_shit}
    end.

%%--------------------------------------------------------------------
%% Internal
%%--------------------------------------------------------------------

perf(UUID, QS) ->
    Zone = wiggle_metrics:short_id(UUID),
    Elems = perf_cpu(Zone) ++ perf_mem(Zone) ++ perf_swap(Zone)
        ++ perf_net(Zone, <<"net0">>) ++ perf_zfs(Zone),
    wiggle_metrics:get(Elems, QS).

perf_cpu(Zone) ->
    [{"cpu-usage",     z([Zone, cpu, usage])},
     {"cpu-effective", z([Zone, cpu, effective])},
     {"cpu-nwait",     z([Zone, cpu, nwait])}].

perf_mem(Zone) ->
    [{"memory-usage", mb([Zone, mem, usage])},
     {"memory-value", mb([Zone, mem, value])}].

perf_swap(Zone) ->
    [{"swap-usage", mb([Zone, swap, usage])},
     {"swap-value", mb([Zone, swap, value])}].

perf_net(Zone, Nic) ->
    [{["net-send-ops-", Nic], der([Zone, net, Nic, opackets64])},
     {["net-recv-ops-", Nic], der([Zone, net, Nic, ipackets64])},
     {["net-send-b-", Nic],  der([Zone, net, Nic, obytes64])},
     {["net-recv-b-", Nic],  der([Zone, net, Nic, ibytes64])}].


perf_zfs(Zone) ->
    [{"zfs-read-b",  der([Zone, zfs, nread])},
     {"zfs-write-b", der([Zone, zfs, nwritten])},
     {"zfs-read-ops", der([Zone, zfs, reads])},
     {"zfs-write-ops", der([Zone, zfs, writes])}].

z(L) ->
    Bkt = application:get_env(wiggle, zone_bucket, zone),
    {m, Bkt, L}.

mb(L) ->
    wiggle_metrics:mb(z(L)).

der(L) ->
    wiggle_metrics:der(z(L)).

verify_file({File, Attrs}, Config) ->
    {ok, Sha1} = jsxd:get([<<"sha1">>], Attrs),
    {ok, D} = fifo_s3_download:new(File, Config),
    CurrentSha1 = calculate_hash(D, crypto:hash_init(sha)),
    {File,
     #{<<"calculated">> => CurrentSha1,
       <<"expected">> => Sha1}}.

calculate_hash(D, H) ->
    case fifo_s3_download:get(D) of
        {ok, done} ->
            base16:encode(crypto:hash_final(H));
        {ok, Data} ->
            calculate_hash(D, crypto:hash_update(H, Data))
    end.

fold_reply(Acc, {exit_status, E}) ->
    {ok, E, Acc};
fold_reply(Acc, {data, B}) ->
    <<Acc/binary, B/binary>>;
fold_reply(Acc, _) ->
    Acc.

end_end_error(E, Start, State, Req, Fmt, Args) ->
    ?MSniffle(?P(State), Start),
    ?ERROR(Fmt, Args),
    {ok, Req1} = cowboy_req:reply(500, Req),
    State2 = wiggle_audit:fail(E, State),
    {halt, Req1, State2}.

can_use_rules([], _Token) ->
    true;
can_use_rules(_, Token) ->
    libsnarl:allowed(Token, [<<"cloud">>, <<"vms">>, <<"advanced_create">>]).
