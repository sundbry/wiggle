%% Feel free to use, reuse and abuse the code in this file.

%% @doc Hello world handler.
-module(wiggle_grouping_h).
-include("wiggle.hrl").

-define(CACHE, grouping).
-define(LIST_CACHE, grouping_list).
-define(FULL_CACHE, grouping_full_list).

-export([allowed_methods/3,
         init/1,
         get/1,
         permission_required/2,
         read/2,
         create/3,
         write/3,
         delete/2]).

-behaviour(wiggle_rest_h).

init(S = #state{path = [?UUID(Grouping)| _]}) ->
    wiggle_audit:set(<<"grouping">>, Grouping, S);

init(S) ->
    S.

allowed_methods(_Version, _Token, [?UUID(_Grouping), <<"metadata">>|_]) ->
    [<<"PUT">>, <<"DELETE">>];

allowed_methods(_Version, _Token, [?UUID(_Grouping), <<"config">>|_]) ->
    [<<"PUT">>, <<"DELETE">>];

allowed_methods(_Version, _Token, [?UUID(_Grouping), <<"elements">>, _]) ->
    [<<"PUT">>, <<"DELETE">>];

allowed_methods(_Version, _Token, [?UUID(_Grouping), <<"groupings">>, _]) ->
    [<<"PUT">>, <<"DELETE">>];

allowed_methods(_Version, _Token, []) ->
    [<<"GET">>, <<"POST">>];

allowed_methods(_Version, _Token, [?UUID(_Grouping)]) ->
    [<<"GET">>, <<"PUT">>, <<"DELETE">>].

get(State = #state{path = [?UUID(Grouping) | _]}) ->
    Start = erlang:system_time(micro_seconds),
    R = case application:get_env(wiggle, grouping_ttl) of
            {ok, {TTL1, TTL2}} ->
                wiggle_h:timeout_cache_with_invalid(
                  ?CACHE, Grouping, TTL1, TTL2, not_found,
                  fun() -> ls_grouping:get(Grouping) end);
            _ ->
                ls_grouping:get(Grouping)
        end,
    ?MSniffle(?P(State), Start),
    R;

get(_State) ->
    not_found.

permission_required(get, []) ->
    {ok, [<<"cloud">>, <<"groupings">>, <<"list">>]};

permission_required(post, []) ->
    {ok, [<<"cloud">>, <<"groupings">>, <<"create">>]};

permission_required(get, [?UUID(Grouping)]) ->
    {ok, [<<"groupings">>, Grouping, <<"get">>]};

permission_required(delete, [?UUID(Grouping)]) ->
    {ok, [<<"groupings">>, Grouping, <<"delete">>]};

permission_required(put, [?UUID(_Grouping)]) ->
    {ok, [<<"cloud">>, <<"groupings">>, <<"create">>]};

permission_required(put, [?UUID(Grouping), <<"elements">>,  _]) ->
    {ok, [<<"groupings">>, Grouping, <<"edit">>]};

permission_required(delete, [?UUID(Grouping), <<"elements">>, _]) ->
    {ok, [<<"groupings">>, Grouping, <<"edit">>]};

permission_required(put, [?UUID(Grouping), <<"groupings">>,  _]) ->
    {ok, [<<"groupings">>, Grouping, <<"edit">>]};

permission_required(delete, [?UUID(Grouping), <<"groupings">>, _]) ->
    {ok, [<<"groupings">>, Grouping, <<"edit">>]};

permission_required(put, [?UUID(Grouping), <<"metadata">> | _]) ->
    {ok, [<<"groupings">>, Grouping, <<"edit">>]};

permission_required(delete, [?UUID(Grouping), <<"metadata">> | _]) ->
    {ok, [<<"groupings">>, Grouping, <<"edit">>]};

permission_required(put, [?UUID(Grouping), <<"config">> | _]) ->
    {ok, [<<"groupings">>, Grouping, <<"edit">>]};

permission_required(delete, [?UUID(Grouping), <<"config">> | _]) ->
    {ok, [<<"groupings">>, Grouping, <<"edit">>]};

permission_required(_Method, _Path) ->
    undefined.

%%--------------------------------------------------------------------
%% GET
%%--------------------------------------------------------------------

read(Req, State = #state{path = []}) ->
    wiggle_h:list(<<"groupings">>,
                  fun ls_grouping:stream/3,
                  fun ft_grouping:uuid/1,
                  fun ft_grouping:to_json/1,
                  Req, State);

read(Req, State = #state{path = [?UUID(_Grouping)], obj = Obj}) ->
    State1 = wiggle_audit:sets(<<"action">>, <<"get">>, State),
    {ft_grouping:to_json(Obj), Req, State1}.

%%--------------------------------------------------------------------
%% PUT
%%--------------------------------------------------------------------

create(Req, State = #state{path = [], version = Version, token=Token},
       #{<<"name">> := Name, <<"type">> := TypeS} = Data) ->
    Type = case TypeS of
               <<"cluster">> ->
                   cluster;
               <<"stack">> ->
                   stack;
               _ ->
                   none
           end,
    Start = erlang:system_time(micro_seconds),
    State1 = wiggle_audit:sets([{<<"action">>, <<"create">>},
                                {<<"type">>, TypeS},
                                {<<"name">>, Name}], State),
    case ls_grouping:add(Name, Type) of
        {ok, UUID} ->
            e2qc:teardown(?LIST_CACHE),
            e2qc:teardown(?FULL_CACHE),
            {ok, User} = ls_user:get(Token),
            State2 = wiggle_audit:sets(<<"grouping">>, UUID, State1),
            State3 = case ft_user:active_org(User) of
                         <<Org:36/binary>> ->
                             ls_org:execute_trigger(Org, grouping_create, UUID),
                             wiggle_audit:sets(<<"org">>, Org, State2);
                         _ ->
                             State2
                     end,
            ?MSniffle(?P(State), Start),
            {{true, <<"/api/", Version/binary, "/groupings/", UUID/binary>>},
             Req, State3#state{body = Data}};
        duplicate ->
            ?MSniffle(?P(State), Start),
            {ok, Req1} = cowboy_req:reply(409, Req),
            State2 = wiggle_audit:fail("duplicate", State1),
            {halt, Req1, State2}
    end.

write(Req, State = #state{path = [?UUID(Grouping), <<"elements">>, Element]},
      _Data) ->
    Start = erlang:system_time(micro_seconds),
    State1 = wiggle_audit:sets([{<<"action">>, <<"add_element">>},
                                {<<"vm">>, Element}], State),
    Res = ls_grouping:add_element(Grouping, Element),
    handle_grouping_reply(Res, Start, Req, State1);

write(Req, State = #state{path = [?UUID(Grouping), <<"groupings">>, Element]},
      _Data) ->
    Start = erlang:system_time(micro_seconds),
    State1 = wiggle_audit:sets([{<<"action">>, <<"add_element">>},
                                {<<"sub_grouping">>, Element}], State),
    Res = ls_grouping:add_grouping(Grouping, Element),
    handle_grouping_reply(Res, Start, Req, State1);

write(Req, State = #state{path = [?UUID(Grouping), <<"metadata">> | Path]},
      O) when is_map(O) ->
    [{K, V}] = maps:to_list(O),
    Start = erlang:system_time(micro_seconds),
    ok = ls_grouping:set_metadata(Grouping, [{Path ++ [K], V}]),
    e2qc:evict(?CACHE, Grouping),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    {true, Req, State};

write(Req, State = #state{path = [?UUID(Grouping), <<"config">> | Path]},
      O) when is_map(O) ->
    [{K, V}] = maps:to_list(O),
    Start = erlang:system_time(micro_seconds),
    FullKey = Path ++ [K],
    Reply = ls_grouping:set_config(Grouping, [{FullKey, V}]),
    e2qc:evict(?CACHE, Grouping),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    State1 = wiggle_audit:sets([{<<"action">>, <<"set_config">>},
                                {<<"key">>, FullKey},
                                {<<"value">>, V}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2}.

%%--------------------------------------------------------------------
%% DEETE
%%--------------------------------------------------------------------

delete(Req, State = #state{path = [?UUID(Grouping), <<"metadata">> | Path]}) ->
    Start = erlang:system_time(micro_seconds),
    ok = ls_grouping:set_metadata(Grouping, [{Path, delete}]),
    e2qc:evict(?CACHE, Grouping),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    {true, Req, State};

delete(Req, State = #state{path = [?UUID(Grouping), <<"config">> | Path]}) ->
    Start = erlang:system_time(micro_seconds),
    Reply = ls_grouping:set_config(Grouping, [{Path, delete}]),
    e2qc:evict(?CACHE, Grouping),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    State1 = wiggle_audit:sets([{<<"action">>, <<"delete_config">>},
                                {<<"path">>, Path}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2};

delete(Req,
       State = #state{path = [?UUID(Grouping), <<"elements">>, Element]}) ->
    Start = erlang:system_time(micro_seconds),
    Reply = ls_grouping:remove_element(Grouping, Element),
    e2qc:evict(?CACHE, Grouping),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    State1 = wiggle_audit:sets([{<<"action">>, <<"delete_vm">>},
                                {<<"vm">>, Element}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2};

delete(Req,
       State = #state{path = [?UUID(Grouping), <<"groupings">>, Element]}) ->
    Start = erlang:system_time(micro_seconds),
    Reply = ls_grouping:remove_grouping(Grouping, Element),
    e2qc:evict(?CACHE, Grouping),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    State1 = wiggle_audit:sets([{<<"action">>, <<"delete_grouping">>},
                                {<<"sub_grouping">>, Element}], State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2};

delete(Req, State = #state{path = [?UUID(Grouping)]}) ->
    Start = erlang:system_time(micro_seconds),
    Reply = ls_grouping:delete(Grouping),
    e2qc:evict(?CACHE, Grouping),
    e2qc:teardown(?LIST_CACHE),
    e2qc:teardown(?FULL_CACHE),
    ?MSniffle(?P(State), Start),
    State1 = wiggle_audit:sets(<<"action">>, <<"delete">>, State),
    State2 = wiggle_audit:maybe_fail(Reply, State1),
    {Reply =:= ok, Req, State2}.

handle_grouping_reply(ok, Start, Req, State = #state{path = [Grouping | _ ]}) ->
            e2qc:evict(?CACHE, Grouping),
            e2qc:teardown(?FULL_CACHE),
            ?MSniffle(?P(State), Start),
            {true, Req, State};
handle_grouping_reply(Error, Start, Req, State) ->
            ?MSniffle(?P(State), Start),
            State1 = wiggle_audit:fail(Error, State),
            {false, Req, State1}.
