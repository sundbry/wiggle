-module(wiggle_app).

-behaviour(application).

-include("wiggle.hrl").
-include("wiggle_version.hrl").

%% Application callbacks
-export([start/2, stop/1]).

-export([dispatches/0]).
-ignore_xref([dispatches/0]).
%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    load_schemas(),
    wiggle_sup:start_link().

stop(_State) ->
    ok.

load_schemas() ->
    FileRegexp = ".*\.json$",
    Schemas = filelib:fold_files(
                code:priv_dir(wiggle), FileRegexp, true,
                fun(File, Acc) ->
                        lager:info("[schema] Loading file: ~s~n", [File]),
                        BaseName = filename:basename(File),
                        Key = list_to_atom(filename:rootname(BaseName)),
                        {ok, Bin} = file:read_file(File),
                        JSX = jsone:decode(Bin),
                        ok = jesse:add_schema(Key, JSX),
                        [Key | Acc]
                end, []),
    lager:info("[schemas] Loaded schemas: ~p", [Schemas]).

dispatches() ->
    API = application:get_env(wiggle, api, all),
    UIDir = case application:get_env(wiggle, ui_path) of
                {ok, D} ->
                    D;
                _ ->
                    undefined
            end,
    wiggle:dispatches(API, UIDir).
